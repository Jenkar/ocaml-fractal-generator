open Graphics;;
open Inside_stuff;;
open Graphic_stuff;;
open Crisscross;;
open Parser;;

(* deux fonctions qui permettent d'écrire de longs systèmes plus *)
(* brièvement.                                                   *)

(* convertir des caractères en constantes du type bracketed *)
let tree_of_char = function
  | 'L' -> S L
  | 'R' -> S R
  | '+' -> S P
  | '-' -> S M
  | c -> failwith ("Unknown character: "^(String.make 1 c))
;;

(* convertir une chaîne sans parenthèses en un bracketed *)
let tree_of_string s =
  let rec tree_of_string_aux i =
    if i = String.length s
    then []
    else (tree_of_char (String.get s i))::(tree_of_string_aux (i+1))
  in Seq (tree_of_string_aux 0)
;;


(***********************************************************************)
(* Examples tirés du livre "The Algorithmic Beauty of Plants".         *)
(* Chaque example consiste en un axiome, un système de réécriture, et  *)
(* une interprétation.                                                 *)

(* snow flake  - Figure 3 du sujet *)
let w_snow = Seq [S A;S B;S A;S B;S A]
and s_snow = [A, Seq [S A;S C;S A;S B;S A;S C;S A]]
and i_snow =
  [(A,[Line(30)]); (B,[Turn(120)]); (C,[Turn(-60)]); (D,[Turn(60)])];;

(* quadratic koch island *)
let w_koch = Seq [S A;S B;S A;S B;S A;S B;S A]
and s_koch =
  [A,
   Seq ([S A;S C;S A;S A;S B;S A;S A;S B;S A;S B;S A;S C;S A;S C;S A] @
	   [S A;S B;S A;S B;S A;S C;S A;S C;S A;S A;S C;S A;S A;S B;S A])]
and i_koch =
  [(A,[Line(10)]); (B,[Turn(90)]); (C,[Turn(-90)]); (D,[Move(10)])];;

(* koch squares and lakes *)
let w_koch1 = Seq [S A;S C;S A;S C;S A;S C;S A]
and s_koch1 =
  [A,
   Seq ([S A;S C;S D;S B;S A;S A;S C;S A;S C;S A;S A;S C;S A;S D;S C;S A;S A] @
	   [S B;S D;S C;S A;S A;S B;S A;S B;S A;S A;S B;S A;S D;S B;S A;S A] @
	   [S A]);
   D,
   Seq [S D;S D;S D;S D;S D;S D]];;

(* koch variants *)
let s_koch2 =
  [A, Seq [S A;S A;S B;S A;S B;S A;S B;S A;S B;S A;S B;S A;S C;S A]]
and s_koch3 =
  [A, Seq [S A;S A;S B;S A;S B;S A;S B;S A;S B;S A;S A]]
and s_koch4 =
  [A,Seq [S A;S A;S B;S A;S C;S A;S B;S A;S B;S A;S A]]
and s_koch5 =
  [A,Seq [S A;S A;S B;S A;S B;S B;S A;S B;S A]]

(* The infamous Dragon curve, the terror of IF1. ABOP page 11 *)
let w_dragon =  Seq [S A]
and s_dragon = [
  (A, Seq [S A; S C; S B; S C]);
  (B, Seq [S D; S A; S D; S B])
]
and i_dragon = [(A,[Line(10)]);(B,[Line(10)]);(C,[Turn(90)]);(D,[Turn(-90)])];;

(* Sierpinski Gasket. ABOP page 11 *)
let w_sierp = Seq [S B]
and s_sierp = [
  (A, Seq [S B; S C; S A; S C; S B]);
  (B, Seq [S A; S D; S B; S D; S A])
]
and i_sierp = [(A,[Line(6)]);(B,[Line(6)]);(C,[Turn(60)]);(D,[Turn(-60)])]
;;

(* Hexagonal Gosper curve. ABOP page 12 *)
let w_6gosp = S L
and s_6gosp = [
  (L, tree_of_string "L+R++R-L--LL-R+");
  (R, tree_of_string "-L+RR++R+L--L-R")
]
and i_6gosp =[(L,[Line(4)]);(R,[Line(4)]);(P,[Turn(60)]);(M,[Turn(-60)])]
;;

(* Quadratic Gosper curve. ABOP page 12 *)
let w_4gosp = tree_of_string "-R"
and s_4gosp = [
  (L, tree_of_string "LL-R-R+L+L-R-RL+R+LLR-L+R+LL+R-LR-R-L+L+RR-");
  (R, tree_of_string "+LL-R-R+L+LR+L-RR-L-R+LRR-L-RL+L+R-R-L+L+RR")
]
and i_4gosp = [(L,[Line(2)]);(R,[Line(2)]);(P,[Turn(90)]);(M,[Turn(-90)])]

(* Branching 1. ABOP page 25 *)
let w_br1 = S F
and s_br1 = [
  (F, Seq [S F; Branch [S P; S F]; S F; Branch [S M; S F]; S F])
]
and i_br1 = [(F,[Line(5)]);(P,[Turn(25)]);(M,[Turn(-25)])]

(* Branching 2. ABOP page 25 *)
let w_br2 = S F
and s_br2 = [
  (F, Seq [S F; Branch [S P; S F]; S F; Branch [S M; S F]; Branch [S F]])
]
and i_br2 = [(F,[Line(5)]);(P,[Turn(20)]);(M,[Turn(-20)])]

let examples = [
  "snow",  (w_snow, s_snow, i_snow) ;
  "koch",  (w_koch, s_koch, i_koch) ;
  "koch1", (w_koch1, s_koch1, i_koch) ;
  "koch2", (w_koch, s_koch2, i_koch) ;
  "koch3", (w_koch, s_koch3, i_koch) ;
  "koch4", (w_koch, s_koch4, i_koch) ;
  "koch5", (w_koch, s_koch5, i_koch);
  "dragon", (w_dragon, s_dragon, i_dragon);
  "sierp", (w_sierp, s_sierp, i_sierp);
  "6gosp", (w_6gosp, s_6gosp, i_6gosp);
  "4gosp", (w_4gosp, s_4gosp, i_4gosp);
  "br1",   (w_br1,s_br1,i_br1);
  "br2",   (w_br2,s_br2,i_br2);
]
;;

let rec affiche_exemples l = match l with
  | (a,b)::l2 -> begin
		 print_string (a^"\n");
		 affiche_exemples l2
	       end
  | [] -> print_string ""
;;

let axiom = Seq [S F];;
let s_p = [(F, Seq[S F;Branch[S P; S F]; S F;Branch[S M; S F]; S F])];;
let i_p = [(F,[Line (2)]);(P,[Turn (25)]);(M,[Turn (-25)])];;


let intel_graph abs ord iter word rul inter height length=
  let rec sub n =
    open_graph (" "^(string_of_int length)^"x"^(string_of_int height));
    let t = { x = abs; y = ord; angle = 0} in 
      moveto t.x t.y;
      intel_calculate n word rul inter t;
      if (n < iter)
      then begin
	  wait_next_event [Button_down]; sub (n+1)
      end
      else ()
  in sub 0
;;

let rec inside_main abs ord iter word rul inter height length = 
  let toto = read_line () in
    match toto with
      | ";;" -> inside_main abs ord iter word rul inter height length
      | ("Quit;;" | "Quit") -> ()
      | ("Afficher exemples;;" | "Afficher exemples" | "aff exs" | "aff exs;;") ->
	 affiche_exemples examples;
	 inside_main abs ord iter word rul inter height length			  
      | ("Afficher axiome;;" | "Afficher axiome" | "aff axm" | "aff axm;;") -> 
	  print_string ((affiche word)^"\n"); 
	  inside_main abs ord iter word rul inter height length
      | ("Afficher regle;;" | "Afficher regle" | "aff rg" | "aff rg;;") ->
	  print_string ((affiche_rewrite rul)^"\n");
	  inside_main abs ord iter word rul inter height length
      | ("Afficher interpretation;;" | "Afficher interpretation" | "aff itp" | "aff itp;;") -> 
	  print_string ((affiche_inter inter)^"\n"); 
	  inside_main abs ord iter word rul inter height length
      | ("Afficher nombre d'iterations;;" | "Afficher nombre d'iterations" | "aff iter" | "aff iter;;") ->
	  print_int iter; print_newline ();
	  inside_main abs ord iter word rul inter height length
      | ("Afficher hauteur;;" | "Afficher hauteur" | "aff h" | "aff h;;") ->
	  print_int height; print_newline ();
	  inside_main abs ord iter word rul inter height length
      | ("Afficher longueur;;" | "Afficher longueur" | "aff l" | "aff l;;") ->
	  print_int length; print_newline ();
	  inside_main abs ord iter word rul inter height length
      | ("Afficher abscisse;;" | "Afficher abscisse" | "aff abs;;" | "aff abs") ->
	 print_int abs; print_newline ();
	 inside_main abs ord iter word rul inter height length
      | ("Afficher ordonnee;;" | "Afficher ordonnee" | "aff ord;;" | "aff ord") ->
	 print_int ord; print_newline ();
	 inside_main abs ord iter word rul inter height length
      | ("Calculer;;" | "Calculer" | "calc;;" | "calc") -> 
	  print_string "Pour passer à l'iteration suivante; faire un clic gauche de la souris\n"; 
	  intel_graph abs ord iter word rul inter height length ;
	  print_string "Calcul terminé\n";
	  inside_main abs ord iter word rul inter height length
      | ("Ajuster exemple;;" | "Ajuster exemple" | "aj ex" | "aj ex;;") -> begin
	  print_string "Veuillez entrer un exemple\n";
	  try
	    let (axm,rg,itp) =  List.assoc (read_line ()) examples in
	    print_string "Exemple choisi\n";
	    inside_main abs ord iter axm rg itp height length
	  with Not_found -> print_string "Aucun exemple trouvé. Utilisez \"Afficher exemples\" pour afficher une liste des exemples\n";
			    inside_main abs ord iter word rul inter height length
	  end
					    		   
      | ("Ajuster axiome;;" | "Ajuster axiome" | "aj axm" | "aj axm;;") -> begin
	  print_string "Veuillez entrer le nouvel axiome\n";
	  try 
	    let tmp = simpler_syntax (read_line ()) in
	    print_string "Nouvel axiome enregistré\n";
	    inside_main abs ord iter tmp rul inter height length
	  with Failure s -> print_string s; inside_main abs ord iter word rul inter height length
	end
      | ("Ajuster regle;;" | "Ajuster regle" | "aj rg" | "aj rg;;") -> begin
	  print_string "Veuillez entrer la nouvelle regle de réecriture\n";
	  try 
	    let tmp = get_rule (read_line ()) in
	    print_string "Nouvelle règle enregistrée\n";
	    inside_main abs ord iter word tmp inter height length
	  with Failure s -> print_string s; inside_main abs ord iter word rul inter height length
	end
      | ("Ajuster interpretation;;" | "Ajuster interpretation" | "aj itp" | "aj itp;;") -> begin
	  print_string "Veuillez entrer la nouvelle interpretation\n";
	  try 
	    let tmp = get_inter (read_line ()) in
	    print_string "Nouvelle interprétation enregistrée\n";
	    inside_main abs ord iter word rul tmp height length
	  with Failure s -> print_string s; inside_main abs ord iter word rul inter height length
	end
      | ("Ajuster nombre d'iterations;;" | "Ajuster nombre d'iterations" | "aj iter" | "aj iter;;") -> 
	  print_string "Veuillez entrer le nouveau nombre d'iterations\n"; 
	  begin
	    try
	    let tmp = read_int() in
	    print_string "Nouveau nombre d'itérations enregistré\n";
	    inside_main abs ord tmp word rul inter height length 
	    with Failure s -> print_string "Vous n'avez pas entré un entier"; inside_main abs ord iter word rul inter height length
	  end
      | ("Ajuster hauteur;;" | "Ajuster hauteur" | "aj h" | "aj h;;") -> 
	  print_string "Veuillez entrer la nouvelle hauteur\n"; 
	  begin
	    try 
	      let tmp = read_int () in
	      print_string "Nouvelle hauteur enregistrée\n";
	      inside_main abs ord iter word rul inter tmp length
	    with Failure s -> print_string "Vous n'avez pas entré un entier"; inside_main abs ord iter word rul inter height length
	  end
      | ("Ajuster longueur;;" | "Ajuster longueur" | "aj l" | "aj l;;") -> 
	  print_string "Veuillez entrer la nouvelle longueur\n"; 
	  begin
	    try 
	      let tmp = read_int () in
	      print_string "Nouvelle longueur enregistrée\n";
	      inside_main abs ord iter word rul inter height tmp
	    with Failure s -> print_string "Vous n'avez pas entré un entier"; inside_main abs ord iter word rul inter height length
	  end
      | ("Ajuster abscissse;;" | "Ajuster abscisse" | "aj abs" | "aj abs;;") -> 
	  print_string "Veuillez entrer la nouvelle abscisse\n"; 
	  begin
	    try 
	      let tmp = read_int () in
	      print_string "Nouvelle abscisse enregistrée\n";
	      inside_main tmp ord iter word rul inter height length
	    with Failure s -> print_string "Vous n'avez pas entré un entier"; inside_main abs ord iter word rul inter height length
	  end
      | ("Ajuster ordonnee;;" | "Ajuster ordonnee" | "aj ord" | "aj ord;;") -> 
	  print_string "Veuillez entrer la nouvelle ordonnée\n"; 
	  begin
	    try 
	      let tmp = read_int () in
	      print_string "Nouvelle ordonnée enregistrée\n";
	      inside_main abs tmp iter word rul inter height length
	    with Failure s -> print_string "Vous n'avez pas entré un entier"; inside_main abs ord iter word rul inter height length
	  end
      | ("Help;;" | "Help" | "?" | "?;;") ->
	  print_string "Les commandes suivantes sont disponibles :\n\n";
	  print_string "Quit : Quitte le programme\n\n";
	  print_string "Afficher exemples / aff exs : Affiche des exemples de L-systèmes prédéfinis (tirés de \"The Algorithmic Beauty of Plants\"\n";
	  print_string "Afficher axiome / aff axm : Affiche l'axiome courant\n";
	  print_string "Afficher regle / aff rg : Affiche la regle de reecriture courante\n";
	  print_string "Afficher interpretation / aff itp : Affiche l'interpretation courante\n";
	  print_string "Afficher nombre d'iterations / aff iter : Affiche le nombre d'iteration courant\n";
	  print_string "Afficher hauteur / aff h : Affiche la hauteur courante\n";
	  print_string "Afficher longueur / aff l : Affiche la longueur courante\n";
	  print_string "Afficher abscisse / aff abs : Affiche l'abscisse de départ\n";
	  print_string "Afficher ordonnee / aff ord : Affiche l'ordonnée de départ\n\n";
	  print_string "Calculer /calc : Calcule les iterations successives avec les parametres courants\n\n";
	  print_string "Ajuster exemple / aj ex : Permet de choisir un L-système prédéfini\n";
	  print_string "Ajuster axiome / aj axm : Permet de changer l'axiome\n";
	  print_string "Ajuster regle / aj rg : Permet de changer la regle de réecriture\n";
	  print_string "Ajuster interpretation / aj itp : Permet de changer l'interpretation\n";
	  print_string "Ajuster nombre d'iterations / aj iter : Permet de changer le nombre d'iterations\n";
	  print_string "Ajuster hauteur / aj h: Permet de changer la hauteur\n";
	  print_string "Ajuster longueur / aj l: Permet de changer la longueur\n";
	  print_string "Ajuster abscisse / aj abs: Permet de changer l'abscisse de départ\n";
	  print_string "Ajuster ordonnee / aj ord: Permet de changer l'ordonnée de départ\n\n";
	  print_string "Help / ? : affiche cette aide\n";
	  print_string "Ces commandes peuvent être également suivies de ;; (compatibilité avec le toplevel)\n";
	  print_string "Pour de l'aide sur la syntaxe utilisée pour définir les systemes, utilisez Help syntaxe\n";
	  inside_main abs ord iter word rul inter height length
      | ("Help syntaxe;;" | "Help syntaxe") ->
	  print_string "La syntaxe utilisée est la suivante :\n-Un symbole est représenté par son constructeur. Par exemple, T. Les symboles peuvent être n'importe quelle lettre majuscule hormis S.\n-Une Branche (Branch en interne) est débutée par un '[', et terminée par un ']'. Entre eux, une Sequence.\n-Une Sequence (Seq en interne) est juste plusieurs symboles/branches qui se suivent, par exemple TBT[TT]A.\n\n";
	  print_string "Les axiomes sont des séquences simples.\nLes regles sont de la forme [(Symbole,[Sequence]);(Symbole,[Sequence])...(Symbole,[Sequence])], par exemple [(A,[ABA]);(B,[B])].\nLes interpretations sont de la forme [(Symbole,[Commande1(Entier);Commande2(Entier);...Commanden(Entier)]);...]. Les commandes dirigent la tortue, et sont au nombre de trois :\n";
	  print_string "-Line(x) : Avance la tortue, en tracant, de x.\n-Move(x) : Avance la tortue de x sans tracer.\n-Turn(x) : Fait tourner la tortue de x radians.\nFin de l'aide sur la syntaxe.\n";
	  inside_main abs ord iter word rul inter height length
      | _ -> print_string "Commande non connue. Utilisez Help pour avoir une liste des commandes\n";
	  inside_main abs ord iter word rul inter height length
;;

let rec main n abs ord word rul inter height length =
try 
  print_string "Des paramètres par défauts ont été créés. Si vous ne savez pas comment les changer, tapez Help, puis 'Return'\n";
  inside_main abs ord n word rul inter height length
with Graphic_failure("fatal I/O error") -> ()
;;

let abs = 500;;
let ord = 350;;
main 5 abs ord axiom s_p i_p 700 1000;;
