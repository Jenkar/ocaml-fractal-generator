open Inside_stuff;;
open Graphic_stuff;;

type interpretation = (symbol * turtle_command list) list;;

let rec interpret_graphic i t = match i with
  | [] -> ()
  | b::ii -> interpret_turtle_command b t; interpret_graphic ii t
;;

let rec intel_calculate n axiom regle inter t = match axiom with
  | S s -> begin
    if (n = 0)
    then interpret_graphic (List.assoc s inter) t
    else intel_calculate (n-1) (itere (S s) regle) regle inter t
  end
  | Seq (s::l) -> intel_calculate n s regle inter t; intel_calculate n (Seq l) regle inter t
  | Branch (s::l) -> begin
    let y = { x = t.x ; y = t.y ; angle = t.angle }
    in begin
      intel_calculate n s regle inter y;
      intel_calculate n (Seq l) regle inter y
    end
  end
  | _ -> ()
;;
