open Inside_stuff;;
open Graphic_stuff;;
open Crisscross;;

let brack_of_char s = match s with
  | 'A' -> S A
  | 'B' -> S B
  | 'C' -> S C
  | 'D' -> S D
  | 'E' -> S E
  | 'F' -> S F
  | 'G' -> S G
  | 'H' -> S H
  | 'I' -> S I
  | 'J' -> S J
  | 'K' -> S K
  | 'L' -> S L
  | 'M' -> S M
  | 'N' -> S N
  | 'O' -> S O
  | 'P' -> S P
  | 'Q' -> S Q
  | 'R' -> S R
  | 'T' -> S T
  | 'U' -> S U
  | 'V' -> S V
  | 'W' -> S W
  | 'X' -> S X
  | 'Y' -> S Y
  | 'Z' -> S Z
  | a -> failwith ("Le caractere \"" ^(String.make 1 a)^"\" ne peut pas être utilisé.\n")
;;

let symb_of_char s = match s with
  | 'A' -> A
  | 'B' -> B
  | 'C' -> C
  | 'D' -> D
  | 'E' -> E
  | 'F' -> F
  | 'G' -> G
  | 'H' -> H
  | 'I' -> I
  | 'J' -> J
  | 'K' -> K
  | 'L' -> L
  | 'M' -> M
  | 'N' -> N
  | 'O' -> O
  | 'P' -> P
  | 'Q' -> Q
  | 'R' -> R
  | 'T' -> T
  | 'U' -> U
  | 'V' -> V
  | 'W' -> W
  | 'X' -> X
  | 'Y' -> Y
  | 'Z' -> Z
  | a -> failwith ("Le caractere \"" ^(String.make 1 a)^"\" ne peut pas être utilisé.\n")
;;

exception Not_allowed;;
exception Mismatch;;

let rec calc_end_branch s i b = 
  if (i >= String.length s)
  then failwith "Parsing error"
  else begin
    if ((String.get s i) = ']')
    then begin
      if (b = 1) 
      then i
      else calc_end_branch s (i+1) (b-1)
    end
    else begin
      if ((String.get s i) = '[')
      then calc_end_branch s (i+1) (b+1)
      else calc_end_branch s (i+1) b
    end
  end
;;

let rec calc_end_paren s i b = 
  if (i >= String.length s)
  then failwith "Parsing error"
  else begin
    if ((String.get s i) = ')')
    then begin
      if (b = 1) 
      then i
      else calc_end_paren s (i+1) (b-1)
    end
    else begin
      if ((String.get s i) = '(')
      then calc_end_paren s (i+1) (b+1)
      else calc_end_paren s (i+1) b
    end
  end
;;

let rec simpler_syntax s = 
  let rec subroutine i = 
    if (String.length s <= i)
    then []
    else match (String.get s i) with 
      | '[' -> Branch (flatten (simpler_syntax (String.sub s (i+1) ((calc_end_branch s (i+1) 1) - i-1))))::(subroutine ((calc_end_branch s (i+1) 1)+1))
      | c -> (brack_of_char c)::(subroutine (i+1))
  in Seq (subroutine 0)
;;

let rec affiche_commandes liste = 
  let rec subroutine = function
    | [] -> ""
    | [c] -> (affiche_commande c)^"]"
    | c::l -> (affiche_commande c)^";"^(subroutine l)
  in "["^(subroutine liste)
;;

let rec affiche_inter inter = 
  let rec subroutine = function
    | [] -> ""
    | [(a,b)] -> "("^(affiche (S a))^","^(affiche_commandes b)^")]"
    | (a,b)::l -> "("^(affiche (S a))^","^(affiche_commandes b)^");"^(subroutine l)
  in "["^(subroutine inter)
;;

let get_command_list s = 
  let rec subroutine i = 
    if (i >= String.length s)
    then []
    else begin
      if (i+6 >= String.length s)
      then failwith "Parsing error"
      else
	match (String.sub s i 4) with
	  | "Line" ->
	      if ((String.get s (i+4)) <> '(')
	      then failwith "Parsing error"
	      else (Line (int_of_string (String.sub s (i+5) ((calc_end_paren s (i+5) 1) - i - 5))))::(subroutine ((calc_end_paren s (i+5) 1) + 2))
	  | "Turn" ->
	      if ((String.get s (i+4)) <> '(')
	      then failwith "Parsing error"
	      else (Turn (int_of_string (String.sub s (i+5) ((calc_end_paren s (i+5) 1) - i - 5))))::(subroutine ((calc_end_paren s (i+5) 1) + 2))
	  | "Move" ->
	      if ((String.get s (i+4)) <> '(')
	      then failwith "Parsing error"
	      else (Move (int_of_string (String.sub s (i+5) ((calc_end_paren s (i+5) 1) - i - 5))))::(subroutine ((calc_end_paren s (i+5) 1) + 2))
	  | _ -> failwith "Parsing error"
    end
  in subroutine 0
;;

let get_inter s = 
  let rec subroutine i = 
    if (i >= (String.length s))
    then []
    else match (String.get s i) with
      | '(' -> begin
	  if (i+3 >= String.length s || String.get s (i+2) <> ',' || String.get s (i+3) <> '[')
	  then failwith "Parsing Error"
	  else ((symb_of_char (String.get s (i+1))),(get_command_list (String.sub s (i+4) ((calc_end_branch s (i+4) 1) - i-4))))::(subroutine ((calc_end_paren s (i+1) 1) + 1))
	end  
      | ';' | ']' -> subroutine (i+1)
      | _ -> failwith "Parsing error"
  in subroutine 1
;;

let get_rule s= 
  let rec subroutine i =
    if (i >= String.length s)
    then []
    else match (String.get s i) with
      | '(' -> begin
	  if (i+3 >= String.length s || String.get s (i+2) <> ',' || String.get s (i+3) <> '[')
	  then failwith "Parsing Error"
	  else ((symb_of_char (String.get s (i+1))),(simpler_syntax (String.sub s (i+4) ((calc_end_branch s (i+4) 1) - i - 4))))::(subroutine ((calc_end_paren s (i+1) 1) +1))
	end 
      | ';' | ']' -> subroutine (i+1)
      | _ -> failwith "Parsing error"
  in subroutine 1
;;
