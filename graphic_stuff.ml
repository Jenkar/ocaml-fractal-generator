open Graphics;;

let nearest x = 
  if (x > 0.)
  then begin
    if (x > (float_of_int (int_of_float x))+. 0.5)
    then (int_of_float x) + 1
    else (int_of_float x)
  end
  else begin
    if (x < (float_of_int (int_of_float x))-. 0.5)
    then (int_of_float x) - 1
    else (int_of_float x)
  end
;;
(* Turtle part starts here *)
let pi = 3.14159265;;
type turtle_command = Move of int
		      | Line of int
		      | Turn of int
;;

let affiche_commande c = match c with
  | Move x -> "Move("^(string_of_int x)^")"
  | Line x -> "Line("^(string_of_int x)^")"
  | Turn x -> "Turn("^(string_of_int x)^")"
;;

type turtle = { mutable x: int;
  mutable y: int;
  mutable angle: int;
};;

let interpret_turtle_command turtle_command turtle = match turtle_command with
  | Move (len) -> begin 
    turtle.x <- turtle.x + (nearest (((cos (((float_of_int turtle.angle)*.(2.0*.pi))/.360.0))*.(float_of_int len))));
    turtle.y <- turtle.y + (nearest (((sin (((float_of_int turtle.angle)*.(2.0*.pi))/.360.0))*.(float_of_int len))));
    end
  | Line (len) -> begin
    turtle.x <- turtle.x + (nearest (((cos (((float_of_int turtle.angle)*.(2.0*.pi))/.360.0))*.(float_of_int len))));
    turtle.y <- turtle.y + (nearest (((sin (((float_of_int turtle.angle)*.(2.0*.pi))/.360.0))*.(float_of_int len))));
    lineto turtle.x turtle.y
    end
  | Turn (ang) -> turtle.angle <- ((turtle.angle + ang) mod 360)
;;
