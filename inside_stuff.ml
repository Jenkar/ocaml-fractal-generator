type symbol = A | B | C | D | E | F | G | H | I | J | K | L | M | N | O | P| Q | R | T | U | V | W | X | Y | Z;;

type bracketed = S of symbol
| Seq of bracketed list
| Branch of bracketed list;;
let l1 = Seq [(S F);
	      Branch [(S P);
		      Branch [(S P); (S F)];
		      (S F)]]
;;

type rewriting_system = (symbol * bracketed) list;;

let chaine = Seq [S A ; Seq [ S B; S D]];;

let rewrite = [ (A, Seq [S A; S B; S A] ) ; (B, Seq [S B; S B; S B] )];;

let rec affiche x = match x with
  | S s -> begin
    match s with
    | A -> "A"
    | B -> "B"
    | C -> "C"
    | D -> "D"
    | E -> "E"
    | F -> "F"
    | G -> "G"
    | H -> "H"
    | I -> "I"
    | J -> "J"
    | K -> "K"
    | L -> "L"
    | M -> "M"
    | N -> "N"
    | O -> "O"
    | P -> "P"
    | Q -> "Q"
    | R -> "R"
    | T -> "T"
    | U -> "U"
    | V -> "V"
    | W -> "W"
    | X -> "X"
    | Y -> "Y"
    | Z -> "Z"
  end
  | Seq (a::r) -> (affiche a) ^ (affiche (Seq r))
  | Branch (a::r) -> "[" ^ (affiche a) ^ (affiche (Seq r)) ^ "]"
  | _ -> ""
;;

let affiche_rewrite r =
  let rec subroutine = function
    | [] -> ""
    | [(a,b)] -> "("^(affiche (S a))^",["^(affiche b)^"])]"
    | (a,b)::l -> "("^(affiche (S a))^",["^(affiche b)^"]);"^(subroutine l)
  in "["^(subroutine r)
;;

let flatten bracketed = match bracketed with 
  | Seq s -> s
  | x -> [x]
;;

let concat a1 a2 = match a1 with
  | S s -> begin
    match a2 with
    | S s2 -> Seq [S s;S s2]
    | Seq a -> Seq ((S s)::(flatten(a2)))
    | Branch a -> Seq [S s; Branch a]
  end
  | Seq a -> begin
    match a2 with
    | S s -> Seq (flatten(a1)@[a2])
    | Seq b -> Seq (flatten(a1)@(flatten(a2)))
    | Branch b -> Seq (flatten(a1)@[Branch b])
  end
  | Branch a -> begin
    match a2 with
    | S s -> Seq [Branch a; S s]
    | Seq b -> Seq (a1::(flatten(a2)))
    | Branch b -> Seq [Branch a; Branch b]
  end
;;


let rec itere x regle = match x with
  | S s -> begin 
      try (List.assoc s regle) with Not_found -> S s
    end 
  | Seq (a::r) -> concat (itere a regle) (itere (Seq r) regle)
  | Branch (a::r) -> Branch (flatten (concat (itere a regle) (itere (Seq r) regle)))
  | _ -> Seq []
;;
